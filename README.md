This README file has been based from the [official java image](https://hub.docker.com/r/library/java/)

# Supported tags and respective `Dockerfile` links

- [`8-jdk`, `8`, `8u77-jdk`, `latest`, (*8-jdkDockerfile*)](https://bitbucket.org/aytra/java/commits/e217a778b1cc4ed80378bc2b4ac2a5170970c56a)


# What is Java?

Java is a concurrent, class-based, object-oriented language specifically designed to have as few implementation dependencies as possible. It is intended to allow application developers to "write once, run anywhere", meaning that code that runs on one platform does not need to be recompiled to run on another.

Java is a registered trademark of Oracle and/or its affiliates.

> [wikipedia.org/wiki/Java_(programming_language)](http://en.wikipedia.org/wiki/Java_%28programming_language%29)

![logo](https://raw.githubusercontent.com/docker-library/docs/01c12653951b2fe592c1f93a13b4e289ada0e3a1/java/logo.png)

# How to use this image

## Start a Java instance in your app

The most straightforward way to use this image is to use a Java container as both the build and runtime environment. In your `Dockerfile`, writing something along the lines of the following will compile and run your project:

```dockerfile
FROM aytra/java:8-jdk
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN javac Main.java
CMD ["java", "Main"]
```

You can then run and build the Docker image:

```console
$ docker build -t my-java-app .
$ docker run -it --rm --name my-running-app my-java-app
```

## Compile your app inside the Docker container

There may be occasions where it is not appropriate to run your app inside a container. To compile, but not run your app inside the Docker instance, you can write something like:

```console
$ docker run --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp aytra/java:8-jdk javac Main.java
```

This will add your current directory as a volume to the container, set the working directory to the volume, and run the command `javac Main.java` which will tell Java to compile the code in `Main.java` and output the Java class file to `Main.class`.

# Why not use the official java image?

The [official java image](https://hub.docker.com/r/library/java/) only supports OpenJDK since most Linux distros only support OpenJDK. Some applications might be running with Oracle's JDK and without going into a debate on which one is better the option should be available so users can make their own choices


# Image Variants

## `java:<version>`

This image comes with two distinct flavors. One containing java's SDK and maven targetted for development environments and one containing only java's runtime without maven targetted for production environments.

# License

License for the software contained in this image:
* Oracle Java [license information](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)
* Apache Maven [license information](https://www.apache.org/licenses/)

# Supported Docker versions

This image is officially supported on Docker version 1.10.3.

Support for older versions (down to 1.6) is provided on a best-effort basis.

Please see [the Docker installation documentation](https://docs.docker.com/installation/) for details on how to upgrade your Docker daemon.

# User Feedback

## Documentation

## Issues

## Contributing

You are invited to contribute new features, fixes, or updates, large or small; we are always thrilled to receive pull requests, and do our best to process them as fast as we can.

Before you start to code, we recommend discussing your plans through a [BitBucket issue](https://bitbucket.org/aytra/java/issues), especially for more ambitious contributions. This gives other contributors a chance to point you in the right direction, give you feedback on your design, and help you find out if someone else is working on the same thing.

